from notejam import app

import signal
import json_logging
from healthcheck import HealthCheck
from db import db_available

def on_sigterm(x, y):
    raise SystemExit


if __name__ == '__main__':
    signal.signal(signal.SIGTERM, on_sigterm)
    json_logging.init_flask(enable_json=True)
    json_logging.init_request_instrument(app)

    health = HealthCheck(app, "/healthcheck")
    health.add_check(db_available)
    app.run(host='0.0.0.0', port=15000)

