from notejam import db

# Create db schema
db.create_all()

def db_available():
    db.engine.execute('SELECT 1')
    return True, "ok"